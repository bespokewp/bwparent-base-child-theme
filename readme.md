#Getting started
1. Make sure the parent theme is installed, or [download and install](https://bitbucket.org/dev_lime/base-wordpress-theme/src/master/)
2. Make sure NodeJS is installed, or [download and install ](https://nodejs.org/en/download/) it
3. With NodeJS availbale, from the terminal run `npm install` in the project root directory - where the `package.json` file is visible
4. Run `gulp watch` to monitor changes in the scss files with the gulp watcher
5. Rename the theme in the style.scss