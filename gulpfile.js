////
// Dependencies
////
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');


////
// Frontend styles
////
gulp.task('frontend-styles', function() {

    return sass('style.scss', { style: 'compressed' })
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('./'))
        .pipe(notify({ message: 'Styles task complete' }));

});


////
// Admin styles
////
gulp.task('admin-styles', function() {

    return sass('resources/admin/scss/admin.scss', { style: 'compressed' })
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('assets/admin/css'))
        .pipe(notify({ message: 'Styles task complete' }));

});

////
// Watcher task for CSS/Script compilers
////
gulp.task('watch', function() {

    // Watch main styles scss file
    gulp.watch([
        'style.scss',
        'resources/frontend/theme/scss/*.scss'
    ], ['frontend-styles']);

    // Watch main styles scss file
    gulp.watch([
        'resources/admin/scss/admin.scss',
        'resources/admin/scss/parts/*.scss',
    ], ['admin-styles']);


});

////
// Scripts
////
gulp.task('scripts', function() {

    // make sure these are added in order of dependency
    return gulp.src([
        'resources/frontend/theme/js/scripts.js'
    ])
        .pipe(jshint.reporter('default'))
        .pipe(rename('scripts.min.js'))
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/frontend/js'))
        .pipe(notify({ message: 'Scripts task complete' }));
});

////
// Image compression
////
gulp.task('images', function() {

    return gulp.src('resources/images/**/*')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('assets/images'))
        .pipe(notify({ message: 'Images task complete' }));

});




////
// Clean before live
////
gulp.task('clean', function() {
    return del(['assets', 'style.css']);
});

////
//
////
gulp.task('default', ['clean'], function() {
    gulp.start( 'frontend-styles', 'admin-styles', 'scripts', 'images');
});