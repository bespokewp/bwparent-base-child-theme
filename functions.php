<?php

//use Carbon_Fields\Container as Container;
//use Carbon_Fields\Field as Field;

// example carbon fields func
//function carbon_field_template_func() {
//
//	$container = Container::make( 'post_meta', 'Config' )
//	                                     ->where( 'post_type', '=', 'post' );
//
//	$container->add_fields( array(
//			Field::make('text', 'bwp_test', 'another test')
//		)
//	);
//}
//add_action('after_carbon_init', 'carbon_field_template_func');


add_action( 'wp_enqueue_scripts', 'enqueue_child_styles' );

if ( !function_exists( 'enqueue_child_styles' ) ) :

	function enqueue_child_styles() {
		// child theme stylesheets
		wp_enqueue_style( 'child_styles', fsi( get_stylesheet_directory_uri() ) . 'style.css', array('frameworks', 'parent_styles') );

		// child theme JS
		wp_enqueue_script( 'child_scripts', fsi( get_stylesheet_directory_uri() ) . 'assets/frontend/js/scripts.min.js', array('jquery') );

	}

endif;
